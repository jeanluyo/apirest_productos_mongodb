package com.entregable2.apirest_productos_mongodb.repository;

import com.entregable2.apirest_productos_mongodb.model.Producto;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface ProductoRepository extends MongoRepository<Producto, String> {
}