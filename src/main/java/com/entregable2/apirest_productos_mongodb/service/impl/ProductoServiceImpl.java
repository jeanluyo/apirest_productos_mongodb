package com.entregable2.apirest_productos_mongodb.service.impl;

import com.entregable2.apirest_productos_mongodb.model.Producto;
import com.entregable2.apirest_productos_mongodb.repository.ProductoRepository;
import com.entregable2.apirest_productos_mongodb.service.ProductoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductoServiceImpl implements ProductoService {
    @Autowired
    ProductoRepository productoRepository;

    @Override
    public Producto crearProducto(Producto producto) {
        return this.productoRepository.insert(producto);
    }

    @Override
    public List<Producto> obtenerProductos() {
        return this.productoRepository.findAll();
    }

    @Override
    public Producto obtenerProductoPorId(String id) {
        final Optional<Producto> optional = this.productoRepository.findById(id);
        return optional.isPresent() ? optional.get() : null;
    }

    @Override
    public Producto actualizarProducto(Producto producto) {
        return this.productoRepository.save(producto);
    }

    @Override
    public void borrarProductoPorId(String id) {
        this.productoRepository.deleteById(id);
    }
}
