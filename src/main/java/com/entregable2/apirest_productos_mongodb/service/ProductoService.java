package com.entregable2.apirest_productos_mongodb.service;

import com.entregable2.apirest_productos_mongodb.model.Producto;

import java.util.List;

public interface ProductoService {

    Producto crearProducto(Producto producto);

    List<Producto> obtenerProductos();

    Producto obtenerProductoPorId(String id);

    Producto actualizarProducto(Producto producto);

    void borrarProductoPorId(String id);
}
