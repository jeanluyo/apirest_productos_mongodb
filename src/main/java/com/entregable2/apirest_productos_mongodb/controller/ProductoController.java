package com.entregable2.apirest_productos_mongodb.controller;

import com.entregable2.apirest_productos_mongodb.model.Producto;
import com.entregable2.apirest_productos_mongodb.service.ProductoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("${api.version}/productos")
public class ProductoController {
    @Autowired
    private ProductoService productoService;

    @GetMapping
    public List<Producto> getProductos() {
        return productoService.obtenerProductos();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Producto> getProductoPorId(@PathVariable String id) {
        Producto producto = productoService.obtenerProductoPorId(id);
        if (producto == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } else {
            return ResponseEntity.ok(producto);
        }
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Producto postProductos(@RequestBody Producto producto) {
        return productoService.crearProducto(producto);
    }

    @PutMapping
    public Producto putProductos(@RequestBody Producto productoToUpdate){
        return productoService.actualizarProducto(productoToUpdate);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteProducto(@PathVariable String id){
        productoService.borrarProductoPorId(id);
    }
}
