package com.entregable2.apirest_productos_mongodb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApirestProductosMongodbApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApirestProductosMongodbApplication.class, args);
	}

}
